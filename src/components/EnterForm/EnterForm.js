import { Button, Grid, makeStyles, TextField, Typography } from '@material-ui/core';
import React from 'react';

const useStyles = makeStyles(theme => ({
    center: {
        textAlign: 'center'
    },
    mainBody: {
        maxWidth: "70%",
        margin: theme.spacing(10, 'auto', 5)
    }
}));

const EnterForm = ({ value, onChange, onSubmit }) => {
    const classes = useStyles();

    return (
        <Grid container direction="column" spacing={2} className={classes.mainBody}>
            <Grid item xs>
                <Typography variant="h4" className={classes.center}>Shorten you link!</Typography>
            </Grid>
            <form onSubmit={onSubmit}>
                <Grid container direction='column' spacing={2}>
                    <Grid item xs>
                        <TextField
                            fullWidth
                            variant='outlined'
                            label='Enter URL here'
                            name='originalUrl'
                            value={value}
                            onChange={onChange}
                        />
                    </Grid>
                    <Grid item xs className={classes.center}>
                        <Button type='submit' variant='contained' color='primary'>Shorten</Button>
                    </Grid>
                </Grid>
            </form>

        </Grid>
    );
};

export default EnterForm;