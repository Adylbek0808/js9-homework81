import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { nanoid } from "nanoid";

import EnterForm from '../../components/EnterForm/EnterForm';
import { createShortUrl } from '../../store/actions/actions';
import { apiURL } from '../../config';
import { Grid, Typography } from '@material-ui/core';


const Main = () => {

    const dispatch = useDispatch();
    const submitUrl = async (urlData) => {
        await dispatch(createShortUrl(urlData))

    }
    const [state, setState] = useState({
        originalUrl: '',
        shortUrl: ''
    });

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const submitFormHandler = e => {
        e.preventDefault();
        const urlData = {
            originalUrl: state.originalUrl
        };
        urlData.shortUrl = nanoid()
        submitUrl(urlData);
        setState(urlData)
        console.log(urlData)
    };

    return (
        <>
            <EnterForm
                value={state.originalUrl}
                onChange={inputChangeHandler}
                onSubmit={submitFormHandler}
            />
            {state.shortUrl ? (

                <Grid container direction='column' alignContent='center'>
                    <Grid item xs>
                        <Typography variant='h6' >Shortened link is:</Typography>
                    </Grid>
                    <Grid item xs>
                        <a href={apiURL + '/' + state.shortUrl} target="_blank" rel='noreferrer'>  localhost:8000/{state.shortUrl}  </a>
                    </Grid>
                </Grid>
            ) : (
                <>
                </>
            )
            }
        </>
    );
};

export default Main;