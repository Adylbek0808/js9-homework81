import axiosApi from "../../axiosApi";

export const CREATE_SHORTURL_SUCCESS = "CREATE_SHORTURL_SUCCESS";


export const createShortUrlSuccess = () => ({ type: CREATE_SHORTURL_SUCCESS });


export const createShortUrl = urlData => {
    return async dispatch => {
        await axiosApi.post('/links/', urlData);
        dispatch(createShortUrlSuccess());
    };
};

export const openShortLink = data => {
    return async dispatch =>{
        await axiosApi.get('/',data)
    }
}